// File Name:    Piece.java
// Description:  This is a class to set UI of Ball
package main.reversi.ui;

import javafx.animation.FadeTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import main.reversi.model.Owner;

public class Piece extends ImageView {
  private ObjectProperty<Owner> ownerProperty = new SimpleObjectProperty<Owner>(Owner.NONE);
  //get ownerProperty
  public ObjectProperty<Owner> ownerProperty() {
    return ownerProperty;
  }
  //get Owner not Property
  public Owner getOwner() {
    return ownerProperty.get();
  }
  //set Owner Property by passing owner
  public void setOwner(Owner owner) {
    ownerProperty.set(owner);
  }
  //set looks of ball(3 constructors)
  public Piece(final int x ,final int y) {  
    imageProperty().bind(Bindings.when(ownerProperty.isEqualTo(Owner.NONE))
    .then(new Image("/main/resources/none.png",60,60,false,false))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WHITE))
    		
    .then(new Image("/main/resources/white.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLACK))
 
    .then(new Image("/main/resources/black.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.RED))
    
    .then(new Image("/main/resources/red.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.YELLOW))

    .then(new Image("/main/resources/yellow.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUE))
          						 
    .then(new Image("/main/resources/blue.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.GREEN))
          								
    .then(new Image("/main/resources/green.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.ORANGE))
          										 
    .then(new Image("/main/resources/orange.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BOMB))
    										
   	.then(new Image("/main/resources/Bomb.png",60,60,false,true))
   	.otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WILD))
          													
   	.then(new Image("/main/resources/rainbow.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.REDORANGE))
          						
    .then(new Image("/main/resources/red&orange.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUEWHITE))
          															
    .then(new Image("/main/resources/blue&white.png",60,60,false,true))
            .otherwise(new Image("/main/resources/yellow&green.png", 60,60,false,true))))))))))))));
          																																						
    DropShadow shadow = new DropShadow();
    shadow.setOffsetX(1);
    shadow.setOffsetY(1);
    shadow.setSpread(0.2);
    shadow.setColor(Color.BLACK.brighter());
    this.setEffect(shadow);
    		

  }
  //set balls(constructor)
  public Piece(Owner owner) {
    ownerProperty.setValue(owner);
    imageProperty().bind(Bindings.when(ownerProperty.isEqualTo(Owner.NONE))
    .then(new Image("/main/resources/none.png",60,60,false,false))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WHITE))
    		
    .then(new Image("/main/resources/white.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLACK))
 
    .then(new Image("/main/resources/black.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.RED))
    
    .then(new Image("/main/resources/red.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.YELLOW))

    .then(new Image("/main/resources/yellow.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUE))
          						 
    .then(new Image("/main/resources/blue.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.GREEN))
          								
    .then(new Image("/main/resources/green.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.ORANGE))
          										 
    .then(new Image("/main/resources/orange.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BOMB))
    										
   	.then(new Image("/main/resources/Bomb.png",60,60,false,true))
   	.otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WILD))
          													
   	.then(new Image("/main/resources/rainbow.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.REDORANGE))
          						
    .then(new Image("/main/resources/red&orange.png",60,60,false,true))
    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUEWHITE))
          															
    .then(new Image("/main/resources/blue&white.png",60,60,false,true))
            .otherwise(new Image("/main/resources/yellow&green.png", 60,60,false,true))))))))))))));
    
    DropShadow shadow = new DropShadow();
	shadow.setOffsetX(1);
	shadow.setOffsetY(1);
	shadow.setSpread(0.2);
	shadow.setColor(Color.BLACK.brighter());
	this.setEffect(shadow);
	
  }
  public Piece() {
	    imageProperty().bind(Bindings.when(ownerProperty.isEqualTo(Owner.NONE))
	    	    .then(new Image("/none.png",60,60,false,false))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WHITE))
	    	    		
	    	    .then(new Image("/white.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLACK))
	    	 
	    	    .then(new Image("/black.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.RED))
	    	    
	    	    .then(new Image("/red.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.YELLOW))
	    	    		
	    	    .then(new Image("/yellow.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUE))
	    	          						 
	    	    .then(new Image("/blue.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.GREEN))
	    	          								
	    	    .then(new Image("/green.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.ORANGE))
	    	          										 
	    	    .then(new Image("/orange.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BOMB))
	    	    										
	    	   	.then(new Image("/Bomb.png",60,60,false,true))
	    	   	.otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.WILD))
	    	          													
	    	   	.then(new Image("/rainbow.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.REDORANGE))
	    	          						
	    	    .then(new Image("/red&orange.png",60,60,false,true))
	    	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Owner.BLUEWHITE))
	    	          															
	    	    .then(new Image("/blue&white.png",60,60,false,true))
                        .otherwise(new Image("/yellow&green.png", 60,60,false,true))))))))))))));
	   
	    DropShadow shadow = new DropShadow();
		shadow.setOffsetX(1);
		shadow.setOffsetY(1);
		shadow.setColor(Color.BLACK.brighter());
		this.setEffect(shadow);
		//
		this.setOpacity(0);
		FadeTransition fadeTransition = new FadeTransition(Duration.millis(800), this);
		fadeTransition.setFromValue(0); 
		fadeTransition.setToValue(1); 
		fadeTransition.setDelay(Duration.seconds(1));
		fadeTransition.play( );
	  }

  
}
