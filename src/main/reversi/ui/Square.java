// File Name:    Square.java
// Description:  This is a class to set UI of Background

package main.reversi.ui;

import main.reversi.model.Holder;
import javafx.beans.binding.Bindings;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Square extends ImageView {

	  private ObjectProperty<Holder> ownerProperty =
			  new SimpleObjectProperty<Holder>(Holder.SQUARE);
	  //get ownerProperty
	  public ObjectProperty<Holder> ownerProperty() {
	    return ownerProperty;
	  }
	  //get Owner not Property
	  public Holder getOwner() {
	    return ownerProperty.get();
	  }
	  //set Owner Property by passing owner
	  public void setOwner(Holder holder) {
	    ownerProperty.set(holder);
	  }

	  public Square(){
		
	  }
      public Square(final int x ,final int y) {	  
	    imageProperty().bind(Bindings.when(ownerProperty.isEqualTo(Holder.SQUARE))
	    .then(new Image("/main/resources/holder.png",70,70,true,true))
	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Holder.HIGHLIGHT))
				.then(new Image("/main/resources/highlight.png", 70, 70, true,true))
	      .otherwise(new Image("/main/resources/select.png", 70, 70, true,true))));
      }
      public Square(Holder hold) {	  
	    imageProperty().bind(Bindings.when(ownerProperty.isEqualTo(Holder.SQUARE))
	    .then(new Image("/main/resources/holder.png",70,70,true,true))
	    .otherwise(Bindings.when(ownerProperty.isEqualTo(Holder.HIGHLIGHT))
	      .then(new Image("/main/resources/highlight.png",70,70,true,true))
	      .otherwise(new Image("/main/resources/select.png",70,70,true,true))));
      }
 }

