// File Name:    Owner.java
// Description:  This is an enum class to define all kinds of ball
package main.reversi.model;

public enum Owner {
  NONE,
  WHITE,
  BLACK,
  
  RED,
  YELLOW,
  BLUE,
  ORANGE,
  GREEN,
  
  BLUEWHITE,
  GREENYELLOW,
  REDORANGE,
  
  BOMB,
  WILD;
  //enum random ball
  public static Owner radomBall(){
	  Owner temp[] = new Owner[120];
	  
	  for(int i = 0;i<11;i++) temp[i]= Owner.WHITE;
	  for(int i = 11;i<22;i++) temp[i]= Owner.BLACK;
	  for(int i = 22;i<33;i++) temp[i]= Owner.RED;
	  for(int i = 33;i<44;i++) temp[i]= Owner.YELLOW;
	  for(int i = 44;i<55;i++) temp[i]= Owner.BLUE;
	  for(int i = 55;i<66;i++) temp[i]= Owner.ORANGE;
	  for(int i = 66;i<77;i++) temp[i]= Owner.GREEN;

	  for(int i = 77;i<82;i++) temp[i]= Owner.BLUEWHITE;
	  for(int i = 82;i<87;i++) temp[i]= Owner.GREENYELLOW;
	  for(int i = 87;i<92;i++) temp[i]= Owner.REDORANGE;
	  
	  for(int i = 92;i<96;i++) temp[i]= Owner.WILD;
	  for(int i = 96;i<100;i++) temp[i]= Owner.BOMB;

	  int radom = (int) (Math.random()*100);
	  return temp[radom];
  }
  

}
