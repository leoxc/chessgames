// File Name:    Holder.java
// Description:  This is an enum to define 3 kinds of background
package main.reversi.model;

public enum Holder{
	SQUARE,
	HIGHLIGHT,
	SELECT;
}
